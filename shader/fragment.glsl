#version 450

layout(set=0, binding=0) uniform Data {
    vec2 res;
    float time;
} uniforms;
layout(location=0) out vec4 fragColor;

const int MAX_ITER = 200;
const float MAX_DEPTH = 100.0;
const float EPSILON = 0.0001;

float sceneSDF(in vec3 pos) {
    return length(pos) - 1.0;
}

float getDepth(in vec3 eye, in vec3 dir) {
    float depth = 0.0;
    for (int i=0; i<MAX_ITER; i++) {
        float dist = sceneSDF(eye + depth*dir);
        if (dist < EPSILON) {
            return depth;
        }
        depth += dist;
        if (depth >= MAX_DEPTH) {
            return MAX_DEPTH;
        }
    }
    return MAX_DEPTH;
}

vec3 normVec(in vec3 p) {
    return normalize(vec3(
        sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
        sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
        sceneSDF(vec3(p.x, p.y, p.z  + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z - EPSILON))
    ));
}

void main() {
    vec2 iResolution = uniforms.res;
    float iTime = uniforms.time;
    vec4 fragCoord = gl_FragCoord;

    vec3 cam = vec3(0.0, 0.0, -2.0);
    vec3 camDir = vec3(1.0, 0.0, 0.0);
    vec3 light = vec3(0.0, 0.0, -2.0);
    light.x = sin(iTime);
    light.y = cos(iTime);

    vec2 pos = 2.0*fragCoord.xy/iResolution.xy - 1.0;
    pos.x *= iResolution.x/iResolution.y;
    float scale = 1.5;

    vec3 dir = normalize(vec3(pos.x/scale, pos.y/scale, 1.0));

    float depth = getDepth(cam, dir);
    if (depth < MAX_DEPTH) {
        vec3 n = normVec(cam + depth*dir);
        float s = abs(dot(n, dir));
        s *= abs(dot(n, normalize(light - (cam + depth*dir))));
        s = pow(s, 1.2);
        vec3 spec = vec3(s);
        vec3 diff = vec3(0.0, 0.0, 1.0);
        vec3 color = mix(spec, diff, 0.5);
        fragColor = vec4(color, 1.0);
    } else {
        fragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
